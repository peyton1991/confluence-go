package confluence_go

import (
	"encoding/json"
	"io"
	"strings"
)

type Space struct {
	Key  string `json:"key"`
	Name string `json:"name"`
	Url  string `json:"url"`
	Type string `json:"type"`
}

type ResponseSpacesResult struct {
	Id      int      `json:"id"`
	Result  []*Space `json:"result"`
	Jsonrpc string   `json:"jsonrpc"`
}

func (client *Client) GetSpaces() (spaces []*Space, err error) {
	var jsonRpcRequestBody = &JsonRpcRequestBody{
		Jsonrpc: "2.0",
		Method:  getSpaces,
		Params:  nil,
		Id:      14120,
	}
	response, err := client.JsonRpcRequest(jsonRpcRequestBody)
	if err != nil {
		return
	}
	responseByte, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	var responseSpacesResult = &ResponseSpacesResult{}

	err = json.Unmarshal(responseByte, responseSpacesResult)
	if err != nil {
		return
	}

	return responseSpacesResult.Result, nil
}

func (client *Client) GetQuerySpaces(spaceName string) (spaces []*Space, err error) {
	allSpaces, err := client.GetSpaces()
	if err != nil {
		return
	}
	for k, v := range allSpaces {
		if strings.Contains(v.Name, spaceName) {
			spaces = append(spaces, spaces[k])
		}
	}
	return
}
