package confluence_go

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

type Client struct {
	*http.Client
	ConfluenceApiAddress *string
	UserName             *string
	Password             *string
	PersonalAccessTokens *string
}

func NewClient(cli *http.Client, confluenceApiAddress, userName, password, personalAccessTokens *string) *Client {
	return &Client{cli, confluenceApiAddress, userName, password, personalAccessTokens}
}

// Auth 认证,支持个人token和用户密码2种方式，若2种同时时优先使用个人token方式
func (client *Client) auth(req *http.Request) {
	//Supports unauthenticated access to confluence:
	//if username and token are not set, do not add authorization header
	if client.PersonalAccessTokens != nil {
		req.Header.Set("Authorization", "Bearer "+*client.PersonalAccessTokens)
		fmt.Println("Authorization", "Bearer "+*client.PersonalAccessTokens)
	} else if client.UserName != nil && client.Password != nil {
		req.SetBasicAuth(*client.UserName, *client.Password)
	} else {
		panic("未获得授权信息")
	}
}

// ApiGet 构建请求方法-get
func (client *Client) ApiGet(uri string, header http.Header) (*http.Response, error) {
	return client.request("GET", uri, header, nil)
}

// ApiPost 构建请求方法-post
func (client *Client) ApiPost(uri string, header http.Header, requestBody []byte) (*http.Response, error) {
	if header == nil {
		header = http.Header{}
		header.Add("content-type", "application/json;charset=UTF-8")
	} else {
		header.Add("content-type", "application/json;charset=UTF-8")
	}
	fmt.Println("header:", header.Get("content-type"))
	return client.request("POST", uri, header, requestBody)
}

// ApiPostForm 构建请求方法-post form
func (client *Client) ApiPostForm(uri string, header http.Header, form url.Values) (*http.Response, error) {
	if header == nil {
		header = make(map[string][]string)
		header.Add("content-type", "application/x-www-form-urlencoded")
	} else {
		header.Add("content-type", "application/x-www-form-urlencoded")
	}
	return client.request("POST", uri, header, []byte(form.Encode()))
}

// ApiPut 构建请求方法-put
func (client *Client) ApiPut(uri string, header http.Header, requestBody []byte) (*http.Response, error) {
	return client.request("PUT", uri, header, requestBody)
}

func (client *Client) request(method, uri string, header http.Header, requestBody []byte) (*http.Response, error) {
	fmt.Println(time.Now().Format("2006-01-02 15:04:05") + " info 准备使用" + method + "方法请求url:" + uri + ",请求参数为:" + string(requestBody))
	req, err := http.NewRequest(method, uri, bytes.NewBuffer(requestBody))
	if err != nil {
		fmt.Printf("创建请求错误:%s\n", err)
		return nil, fmt.Errorf("创建请求错误:%s", err)
	}
	fmt.Printf("requestBody:%s\n", string(requestBody))
	//如果外部传入Header则设置之
	for k, v := range header {
		for _, vv := range v {
			req.Header.Add(k, vv)
		}
	}
	//认证
	client.auth(req)
	return http.DefaultClient.Do(req)
}

type JsonRpcRequestBody struct {
	Jsonrpc string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
	Id      int           `json:"id"`
}

func (client *Client) JsonRpcRequest(jsonRpcRequestBody *JsonRpcRequestBody) (response *http.Response, err error) {
	uri := client.getJsonRpcUrl()
	if uri == nil {
		return nil, errors.New("获取访问地址异常")
	}
	requestBody, err := json.Marshal(jsonRpcRequestBody)
	if err != nil {
		return
	}
	response, err = client.ApiPost(*uri, nil, requestBody)
	if err != nil {
		return
	}
	return
}
