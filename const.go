package confluence_go

const (
	getUserByName  = "getUserByName"
	addUser        = "addUser"
	addUserToGroup = "addUserToGroup"
	getSpaces      = "getSpaces"
	getUserGroups  = "getUserGroups"
)
