package confluence_go

import (
	"fmt"
	"net/http"
	"testing"
)

func TestGetUserByName(t *testing.T) {

	//var params []interface{} = make([]interface{}, 1)
	//params[0] = "admin"
	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	user := "admin"
	p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, &user, &p, &token)

	getUserByNameResponse, err := client.GetUserByName("pey1")
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("邮箱地址：%s", getUserByNameResponse.Result.Email)
}

func TestGetUsers(t *testing.T) {

	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	user := "admin"
	p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, &user, &p, &token)
	getUserByNameResponse, err := client.GetUsers("test")
	if err != nil {
		t.Error(err)
		return
	}
	for _, v := range getUserByNameResponse {
		fmt.Printf("------ 用户名：%s的展示名称：%s,邮箱地址：%s ------\n", v.Name, v.FullName, v.Email)
	}
	t.Log("success")

}

func TestAddUser(t *testing.T) {

	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	user := "admin"
	p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, &user, &p, &token)

	addUserSponse, err := client.AddUser("pey1", "测试人员", "1234@ss.com", "123")
	if err != nil {
		t.Error(err)
		return
	}
	if addUserSponse.Error != nil {
		t.Logf("返回错误信息:%s", addUserSponse.Error.Message)
		t.Log("success")
	} else {
		t.Log("success")
	}

}

func TestAddUserToGroup(t *testing.T) {

	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	user := "admin"
	p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, &user, &p, &token)
	responseResult, err := client.AddUserToGroup("pey", "测试组")
	if err != nil {
		t.Error(err)
		return
	}
	if responseResult.Error != nil {
		t.Error(responseResult.Error.Message)
		return
	}
	t.Logf(fmt.Sprint(responseResult.Result))
}

func TestQueryUserGroups0(t *testing.T) {

	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	client := NewClient(&http.Client{}, &addr, nil, nil, &token)
	responseResult, err := client.QueryUserGroups("test")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println("...")
	for _, v := range responseResult {
		t.Logf(fmt.Sprintf("%s\n", v))
	}
	t.Logf("success")
}
