package confluence_go

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"
)

type GetGroupResultLinks struct {
	Self string `json:"self"`
}
type GetGroupResult struct {
	Type  string               `json:"type"`
	Name  string               `json:"name"`
	Links *GetGroupResultLinks `json:"_links"`
}
type GetGroupLinks struct {
	Self    string `json:"self"`
	Base    string `json:"base"`
	Context string `json:"context"`
}
type GetGroup struct {
	Results []*GetGroupResult `json:"results"`
	Start   int               `json:"start"`
	Limit   int               `json:"limit"`
	Size    int               `json:"size"`
	Links   *GetGroupLinks    `json:"_links"`
}

// GatQueryGroup 根据组名查询用户组
func (client *Client) GatQueryGroup(query *string) (groups []*GetGroupResult, err error) {
	if query == nil {
		err = errors.New("未获得查询条件，若查询所有，请传入*")
		return
	}
	results, err := client.GetALlGroup()
	if *query == "*" {
		return results, nil
	}
	for k, v := range results {
		if strings.Contains(v.Name, *query) {
			groups = append(groups, results[k])
			fmt.Printf("inner------ %s组的links：%s ------\n", v.Name, v.Links)
		}
	}
	return
}

// GetALlGroup 获取所有的用户组
func (client *Client) GetALlGroup() (results []*GetGroupResult, err error) {
	uri := client.getAllGroupUrl()

	response, err := client.ApiGet(*uri, nil)
	if err != nil {
		return
	}
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	getGroup := &GetGroup{}
	err = json.Unmarshal(responseBody, getGroup)
	if err != nil {
		return
	}
	results = getGroup.Results
	return
}

type GetGroupMemberResponseProfilePicture struct {
	Path      string `json:"path"`
	Width     int    `json:"width"`
	Height    int    `json:"height"`
	IsDefault bool   `json:"isDefault"`
}

type GetGroupMemberResponseResultLinks struct {
	Self string `json:"self"`
}

type GetGroupMemberResponseExpandable struct {
	Status string `json:"status"`
}

type GetGroupMemberResponseResult struct {
	Type           string                                `json:"type"`
	Username       string                                `json:"username"`
	UserKey        string                                `json:"userKey"`
	ProfilePicture *GetGroupMemberResponseProfilePicture `json:"profilePicture"`
	DisplayName    string                                `json:"displayName"`
	Links          *GetGroupMemberResponseResultLinks    `json:"_links"`
	Expandable     *GetGroupMemberResponseExpandable     `json:"_expandable"`
}

type GetGroupMemberResponseLinks struct {
	Self    string `json:"self"`
	Base    string `json:"base"`
	Context string `json:"context"`
}

type GetGroupMemberResponse struct {
	Results []*GetGroupMemberResponseResult `json:"results"`
	Start   int                             `json:"start"`
	Limit   int                             `json:"limit"`
	Size    int                             `json:"size"`
	Links   *GetGroupMemberResponseLinks    `json:"_links"`
}

// GetGroupMember 获取用户组成员
func (client *Client) GetGroupMember(groupName string) (getGroupMemberResponseResults []*GetGroupMemberResponseResult, err error) {
	uri := client.GetGroupMemberUrl(groupName)

	response, err := client.ApiGet(*uri, nil)
	if err != nil {
		return
	}
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	var getGroupMemberResponse = &GetGroupMemberResponse{}
	err = json.Unmarshal(responseBody, getGroupMemberResponse)
	if err != nil {
		return
	}
	getGroupMemberResponseResults = getGroupMemberResponse.Results
	if len(getGroupMemberResponseResults) == 0 {
		err = errors.New("组内未有成员或者组不存在")
	}
	return
}
