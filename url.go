package confluence_go

import (
	"fmt"
	"net/url"
)

func (client *Client) getJsonRpcUrl() *string {
	uri := fmt.Sprintf("%s%s", *client.ConfluenceApiAddress, "/rpc/json-rpc/confluenceservice-v2")
	return &uri
}

func (client *Client) getAllUserUrl() *string {
	query := url.Values{}
	query.Add("query", "*")
	uri := fmt.Sprintf("%s%s", *client.ConfluenceApiAddress, "/rest/user-management/1.0/users?"+query.Encode())
	return &uri
}

func (client *Client) getAllGroupUrl() *string {
	query := url.Values{}
	query.Add("limit", "10000")
	uri := fmt.Sprintf("%s%s", *client.ConfluenceApiAddress, "/rest/api/group?"+query.Encode())
	return &uri
}

func (client *Client) GetGroupMemberUrl(groupName string) *string {
	uri := fmt.Sprintf("%s%s", *client.ConfluenceApiAddress, fmt.Sprintf("/rest/api/group/%s/member", groupName))
	return &uri
}
