package confluence_go

import (
	"net/http"
	"testing"
)

func TestGetALlGroup(t *testing.T) {
	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	//user := "admin"
	//p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, nil, nil, &token)
	results, err := client.GetALlGroup()
	if err != nil {
		t.Error(err)
		return
	}
	for _, v := range results {
		t.Logf("------ %s组的links：%s ------", v.Name, v.Links)
	}
	t.Log("success")
}

func TestGatQueryGroup(t *testing.T) {
	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	//user := "admin"
	//p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, nil, nil, &token)
	var query = "confluence"
	results, err := client.GatQueryGroup(&query)
	if err != nil {
		t.Error(err)
		return
	}
	for _, v := range results {
		t.Logf("------ %s组的links：%s ------", v.Name, v.Links)
	}
	t.Log("success")
}

func TestGetGroupMember(t *testing.T) {
	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	//user := "admin"
	//p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, nil, nil, &token)
	results, err := client.GetGroupMember("测试")
	if err != nil {
		t.Error(err)
		return
	}
	for _, v := range results {
		t.Logf("------ %s 组的用户：%s ------", v.DisplayName, v.Username)
	}
	t.Log("success")
}
