package confluence_go

import (
	"net/http"
	"testing"
)

func TestGetSpaces(t *testing.T) {
	var token = "OTA2OTA5Mjk2MzQyOnZ+AiNl9I0djEAZDPIjKLOqY29E"
	addr := "http://106.14.186.85:8090"
	//user := "admin"
	//p := "QAZxcft@123"
	client := NewClient(&http.Client{}, &addr, nil, nil, &token)
	spaces, err := client.GetSpaces()
	if err != nil {
		t.Error(err)
		return
	}
	for _, v := range spaces {
		t.Logf("空间%s的key:%s，type：%s", v.Name, v.Key, v.Type)
	}
}
