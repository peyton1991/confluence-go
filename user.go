package confluence_go

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
)

type ResponseErrorInfo struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    string `json:"data"`
}

type GetUserByNameResponseResultInfo struct {
	Key      string `json:"key,omitempty"`
	Name     string `json:"name,omitempty"`
	FullName string `json:"fullname,omitempty"`
	Email    string `json:"email,omitempty"`
	Url      string `json:"url,omitempty"`
}

type GetUserByNameResponse struct {
	Id      int                              `json:"id"`
	Error   *ResponseErrorInfo               `json:"error,omitempty"`
	Result  *GetUserByNameResponseResultInfo `json:"result,omitempty"`
	Jsonrpc string                           `json:"jsonrpc"`
}

type ResponseResult struct {
	Id      int                `json:"id"`
	Error   *ResponseErrorInfo `json:"error,omitempty"`
	Result  interface{}        `json:"result,omitempty"`
	Jsonrpc string             `json:"jsonrpc"`
}

func (client *Client) GetUserByName(query string) (getUserByNameResponse *GetUserByNameResponse, err error) {
	var params = make([]interface{}, 1)
	params[0] = query
	var jsonRpcRequestBody = &JsonRpcRequestBody{
		Jsonrpc: "2.0",
		Method:  getUserByName,
		Params:  params,
		Id:      0,
	}
	response, err := client.JsonRpcRequest(jsonRpcRequestBody)
	if err != nil {
		fmt.Println(err)
		return
	}
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	getUserByNameResponse = &GetUserByNameResponse{}
	err = json.Unmarshal(responseBody, getUserByNameResponse)
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

type GetAllUserInfo struct {
	Username    string `json:"username"`
	DisplayName string `json:"displayName"`
	Active      bool   `json:"active"`
}

type GetAllUserResponse struct {
	Users []*GetAllUserInfo `json:"users"`
}

// GetAllUser 获取符合条件的所有用户信息
func (client *Client) GetUsers(userName string) (getUserByNameResponseResultInfos []*GetUserByNameResponseResultInfo, err error) {
	uri := client.getAllUserUrl()
	if uri == nil {
		err = errors.New("获取访问地址异常")
		return
	}
	header := http.Header{}
	header.Add("Accept", "application/json, text/javascript, */*; q=0.01")
	response, err := client.ApiGet(*uri, header)
	if err != nil {
		return
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}
	getUserByNameResponse := &GetAllUserResponse{}
	err = json.Unmarshal(responseBody, getUserByNameResponse)
	if err != nil {
		return
	}

	for _, v := range getUserByNameResponse.Users {
		if !(strings.Contains(v.Username, userName) || userName == "*") {
			continue
		}
		var getUserByNameResponse = &GetUserByNameResponse{}
		var TmpError error
		getUserByNameResponse, TmpError = client.GetUserByName(v.Username)
		if TmpError != nil {
			continue
		}
		getUserByNameResponseResultInfos = append(getUserByNameResponseResultInfos, getUserByNameResponse.Result)
	}
	return
}

func (client *Client) AddUser(name, fullName, email, password string) (addUserResponseResult *ResponseResult, err error) {

	uri := client.getJsonRpcUrl()
	if uri == nil {
		err = errors.New("获取访问地址异常")
		return
	}
	var getUserByNameResponseResultInfo = &GetUserByNameResponseResultInfo{
		Name:     name,
		FullName: fullName,
		Email:    email,
	}
	var params []interface{} = make([]interface{}, 2)
	params[0] = getUserByNameResponseResultInfo
	params[1] = password

	var jsonRpcRequestBody = &JsonRpcRequestBody{
		Jsonrpc: "2.0",
		Method:  addUser,
		Params:  params,
		Id:      12367,
	}
	response, err := client.JsonRpcRequest(jsonRpcRequestBody)

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}
	addUserResponseResult = &ResponseResult{}
	err = json.Unmarshal(responseBody, addUserResponseResult)
	if err != nil {
		return
	}
	return
}

// AddUserToGroup 组新增用户
func (client *Client) AddUserToGroup(userName, groupName string) (responseResult *ResponseResult, err error) {
	var params []interface{} = make([]interface{}, 2)
	params[0] = userName
	params[1] = groupName
	var jsonRpcRequestBody = &JsonRpcRequestBody{
		Jsonrpc: "2.0",
		Method:  addUserToGroup,
		Params:  params,
		Id:      12150,
	}
	response, err := client.JsonRpcRequest(jsonRpcRequestBody)
	if err != nil {
		return
	}

	responseByte, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	responseResult = &ResponseResult{}
	err = json.Unmarshal(responseByte, responseResult)
	if err != nil {
		return
	}
	return
}

// QueryUserGroups 查询用户所属组
func (client *Client) QueryUserGroups(userName string) (groups []string, err error) {

	var params []interface{} = make([]interface{}, 1)
	params[0] = userName
	var jsonRpcRequestBody = &JsonRpcRequestBody{
		Jsonrpc: "2.0",
		Method:  getUserGroups,
		Params:  params,
		Id:      12153,
	}
	response, err := client.JsonRpcRequest(jsonRpcRequestBody)
	if err != nil {
		return
	}

	responseByte, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	var result = &ResponseResult{}
	err = json.Unmarshal(responseByte, result)
	if err != nil {
		return
	}
	if result.Error != nil {
		err = errors.New(result.Error.Message)
		fmt.Println(err)
		return
	}
	switch reflect.TypeOf(result.Result).Kind() {
	case reflect.Slice, reflect.Array:
		s := reflect.ValueOf(result.Result)
		for i := 0; i < s.Len(); i++ {
			groups = append(groups, s.Index(i).Interface().(string))
		}
	}
	return
}
